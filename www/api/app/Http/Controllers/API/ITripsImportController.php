<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\TripsImportRequest;
use Illuminate\Http\Request;

interface ITripsImportController
{

    /**
     * @api {get} /trips List of trips
     * @apiDescription Get list of Trips.
     *
     * @apiName ImportTrips
     * @apiGroup Trips
     * @apiVersion 1.0.0
     *
     * @apiParam  {Number=0,1} [clear] Clear all data before new import.
     * @apiParam  {File="text/csv"} scv File to import.
     *
     * @apiSuccessExample Success-Response:
     * HTTP/1.1 200 OK
     *{
     *  "status": "success",
     *  "code": 200,
     *  "data": {
     *      "records_total": 3,
     *      "records_processed": 3,
     *      "status": true
     *  }
     *}
     *
     * @apiUse BadRequestException
     *
     */
    public function importCsv(TripsImportRequest $request);
}
