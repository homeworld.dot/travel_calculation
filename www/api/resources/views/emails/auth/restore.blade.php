<table style="width:100%;font-size:17px;">
    <thead>
    <tr>
        <th style="background:#242424 url({{env('SITE_URL')}}/images/watermark.png) no-repeat 20px center;height:80px;"></th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td style="padding:10px 20px;">Здравствуйте, {{ $member->name }}</td>
    </tr>
    <tr>
        <td style="padding:10px 20px;">
            <p>Была инициирована процедура восстановления пароля на <a style="color:#039be5;" href="{{env('SITE_URL')}}" target="_blank">console-bay.com</a><p>
            <p>Ваш новый пароль:</p>
            <p style="border: 1px solid #039be5;padding:10px;float:left;margin:0;text-align:center;">
                <span style="display:inline-block;color:#039be5;">{{ $member->new_password }}</span><br/>
            </p>
        </td>
    </tr>
    <tr>
        <td style="padding:10px 20px;">Вы можете сменить его в настройках своего профиля.</td>
    </tr>
    </tbody>
    <tfoot>
    <tr>
        <td style="background:#242424;height:30px;"></td>
    </tr>
    </tfoot>
</table>


