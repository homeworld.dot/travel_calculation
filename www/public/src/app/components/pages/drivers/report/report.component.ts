import { Component, OnInit, ViewChild } from '@angular/core';

import { MatSort, Sort, MatSortModule } from '@angular/material/sort';
import { MatTableDataSource, MatTableModule } from '@angular/material/table';

import { Config } from '../../../../config/app.config';

import { EventsService } from '../../../../services/events/events.service';
import { RequestsService } from '../../../../services/requests/requests.service';

export interface ReportElement {
  driver_id: number;
  total_minutes_with_passenger: number;
}

@Component({
  selector: 'app-drivers-report',
  templateUrl: './report.component.html',
  styleUrls: ['./report.component.scss']
})
export class ReportComponent implements OnInit {
  dataSource = null;
  displayedColumns: string[] = ['driver_id', 'total_minutes_with_passenger'];

  loading: boolean = false;
  success: boolean = false;
  successMessage: string = '';
  errors: Object = {};

  search = '';
  collection: ReportElement[] = [];

  constructor(
    private config: Config,
    private eventsService: EventsService,
    private requestService: RequestsService,
  ) {
    this.eventsService.getEmitter().subscribe(item => {
      if(item.name === 'search'){
        this.search = item.data;
        this.onFilter(this.search);
      }
    });
  }

  @ViewChild(MatSort) sort: MatSort;

  ngOnInit() {

  }

  onSortChange(sortState: Sort) {
    this.dataSource.sort = this.sort;
  }

  onFilter(filterValue: string) {
    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase();
    this.dataSource.filter = filterValue;
  }

  calculate(){
    let self = this;

    self.loading = true;

    this.requestService.index('/trips/calculate').subscribe(
      response => {
        self.collection = response.data;
        self.dataSource = new MatTableDataSource(self.collection);
        self.loading = false;
      },
      error => {
        self.loading = false;
      }
    );
  }

  download(){
    let self = this;
    self.loading = true;

    this.requestService.downloadFile('/trips/calculate?export=1').subscribe(
      response => {
        const blobUrl = URL.createObjectURL(response);
        const a = document.createElement("a");
        a.href = blobUrl;
        let fileName = "trips_payment";
        a.setAttribute("download", fileName + ".csv");
        document.body.appendChild(a);
        a.click();
        document.body.removeChild(a);
        URL.revokeObjectURL(blobUrl);

        self.loading = false;
      },
      error => {
        self.loading = false;
      }
    );
  }
}
