import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { GuardService } from '../../../services/guard/guard.service';

import { ReportComponent } from './report/report.component';


const routes: Routes = [
  {
    path: 'drivers-report',
    component: ReportComponent,
    canActivate: [GuardService],
    data: {
      breadcrumb: [
        {title: 'Drivers report', url: null}
      ]
    }
  }
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DriversRoutingModule { }
