<table style="width:100%;font-size:17px;">
    <thead>
    <tr>
        <th style="background:#242424 url({{env('SITE_URL')}}/images/watermark.png) no-repeat 20px center;height:80px;"></th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td style="padding:10px 20px;">Здравствуйте, {{ $member->name }}</td>
    </tr>
    <tr>
        <td style="padding:10px 20px;">
            <p>Пользователь {{ $buyer->name }} хочет купить у Вас {{ $product->name }}.<p>
        </td>
    </tr>
    <tr>
        <td style="padding:10px 20px;">Вы можете связаться с {{ $buyer->name }} перейдя по <a style="color:#039be5;" href="{{env('SITE_URL').'profile/messages/new/'.$buyer->uid}}" target="_blank">ссылке</a></td>
    </tr>
    </tbody>
    <tfoot>
    <tr>
        <td style="background:#242424;height:30px;"></td>
    </tr>
    </tfoot>
</table>


