<?php

namespace App\Models;


class Trip extends BaseModel
{
    /**
     * @var string
     */
    protected $table = 'trips';

    /**
     * @var string[]
     */
    protected $fillable = [
        'driver_id',
        'pickup',
        'dropoff'
    ];

    /**
     * @var string[]
     */
    const SEARCH_FIELDS = [
        'driver_id',
        'pickup',
        'dropoff',
    ];
}
