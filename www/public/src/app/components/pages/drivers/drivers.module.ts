import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { MatTableModule } from '@angular/material/table';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from '../../../material.module';

import { NgxPaginationModule } from 'ngx-pagination';
import { NgxContentLoadingModule } from 'ngx-content-loading';

import { DriversRoutingModule } from './drivers-routing.module';

import { ReportComponent } from './report/report.component';

@NgModule({
  declarations: [
    ReportComponent
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA
  ],
  imports: [
    FormsModule,
    CommonModule,
    BrowserAnimationsModule,
    MaterialModule,
    MatTableModule,
    NgxPaginationModule,
    NgxContentLoadingModule,
    DriversRoutingModule
  ]
})
export class DriversModule { }
