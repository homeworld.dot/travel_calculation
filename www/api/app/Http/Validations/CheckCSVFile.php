<?php

namespace App\Http\Validations;

use Closure;

class CheckCSVFile implements CustomRuleInterface
{
    /**
     * @return string
     */
    public function name(): string
    {
        return 'check_csv_file';
    }

    /**
     * @return Closure
     */
    public function test(): Closure
    {

        return function ($field, $value, $attributes, $validator){

            if ($attributes[0] == 'importCsv') {
                $allowMimeTypes = ['text/csv', 'application/csv'];

                if (!in_array($value->getMimeType(), $allowMimeTypes)) {
                    return false;
                }

// Check file size
//                    if(($value->getSize() / 1000) > 20000){
//                        return false;
//                    }

                return true;
            }

            return false;

        };
    }


    /**
     * @return string
     */
    public function errorMessage(): string
    {
        return "Wrong file format.";
    }
}
