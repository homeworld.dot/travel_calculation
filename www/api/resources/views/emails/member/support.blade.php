<table style="width:100%;font-size:17px;">
    <thead>
    <tr>
        <th style="background:#242424 url({{env('SITE_URL')}}images/watermark.png) no-repeat 20px center;height:80px;"></th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td style="padding:10px 20px;">
            <p>Пришло обращение от пользователя {{ $member->name }}.<p>
        </td>
    </tr>
    <tr>
        <td style="padding:10px 20px;">
            <p>Тема обращения: {{$subject}}<p>
            <p>Текст обращения:</p>
            <p style="font-style: italic;padding:15px 20px;float:left;margin:0;text-align:left;">{{ $content }}</p>
        </td>
    </tr>
    </tbody>
    <tfoot>
    <tr>
        <td style="background:#242424;height:30px;"></td>
    </tr>
    </tfoot>
</table>


