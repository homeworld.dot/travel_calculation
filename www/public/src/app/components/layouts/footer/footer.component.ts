import { Component, OnInit } from '@angular/core';

import { EventsService  } from '../../../services/events/events.service';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {

  public isDisabled: boolean = false;

  constructor(
    private eventsService: EventsService
  ) {

    this.eventsService.getEmitter().subscribe(item => {
      if(item.name === 'layouts-disabled'){
        this.isDisabled = item.data;
      }
    });

  }

  ngOnInit() {
  }

}
