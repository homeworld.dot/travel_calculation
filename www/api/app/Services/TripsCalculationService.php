<?php

namespace App\Services;

use App\Exports\TripsPaymentExport;
use App\Models\Trip;
use Illuminate\Support\Carbon;

class TripsCalculationService
{
    /**
     * @return array
     */
    public function calculate(bool $export = false): array
    {
        $data[] = [
            'driver_id',
            'total_minutes_with_passenger'
        ];
        $driverTrips = [];
        $timeZone = 'UTC';

        $query = Trip::query();
        $query->orderBy('driver_id')
              ->orderBy('pickup');
        $query->chunk(100, function ($trips) use (&$data, &$driverTrips, &$timeZone) {
            foreach ($trips as $trip) {
                $startTime = Carbon::parse($trip->pickup, $timeZone);
                $finishTime = Carbon::parse($trip->dropoff, $timeZone);

                $driverTrips[$trip->driver_id][] = [
                    'driver_id' => $trip->driver_id,
                    'start_trip' => $startTime,
                    'finish_trip' => $finishTime
                ];
            }
        });

        foreach ($driverTrips as $tripItems) {
            foreach ($tripItems as $key => $item) {
                $diffMin = 0;
                if ($key > 0) {
                    $lasKey = $key - 1;
                    $lastStartTime = $tripItems[$lasKey]['start_trip'];
                    $lastFinishTime = $tripItems[$lasKey]['finish_trip'];
                    $startTripTimeCheck = $item['start_trip'];
                    if ($startTripTimeCheck->between($lastStartTime, $lastFinishTime)) {
                        $diffMin = $startTripTimeCheck->diffInMinutes($lastFinishTime);
                    }
                }

                $data[$item['driver_id']]['driver_id'] = $item['driver_id'];

                if (isset($data[$item['driver_id']])) {
                    $data[$item['driver_id']]['total_minutes_with_passenger'] += $item['finish_trip']->diffInMinutes($item['start_trip']) - $diffMin;
                } else {
                    $data[$item['driver_id']]['total_minutes_with_passenger'] = $item['finish_trip']->diffInMinutes($item['start_trip']) - $diffMin;
                }
            }
        }

        if(!$export) {
            unset($data[0]);
        }
        return $data;
    }

    /**
     * @param array $data
     * @return TripsPaymentExport
     */
    public function export(array $data): TripsPaymentExport
    {
        $csv = new TripsPaymentExport();
        $csv->setArray($data);

        return $csv;
    }
}
