<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BaseModel extends Model
{
    const CREATED_AT = 'created';
    const UPDATED_AT = 'modified';

    public function getCreatedAttribute($date)
    {
        return date('d-m-Y H:i:s', strtotime($date));
    }

    public function getModifiedAttribute($date)
    {
        return date('d-m-Y H:i:s', strtotime($date));
    }

}
