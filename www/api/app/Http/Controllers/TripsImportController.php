<?php

namespace App\Http\Controllers;

use App\Http\Controllers\API\ITripsImportController;
use App\Http\Requests\TripsImportRequest;
use App\Models\Trip;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class TripsImportController extends Controller implements ITripsImportController
{
    public function importCsv(TripsImportRequest $request)
    {
        $total = 0;
        $processed = 0;
        $message = 'success';
        $clear = $request->has('clear') && $request->get('clear');

        if ($request->hasFile('csv')) {

            $file = $request->file('csv');
            $file = $file->store('csv', ['disk' => 'public']);
            $fileStream = fopen(storage_path('app/public/' . $file), 'r');
            $csvContents = [];

            while (($line = fgetcsv($fileStream)) !== false) {
                $csvContents[] = $line;
            }

            fclose($fileStream);
            Storage::delete($file);
            unset($csvContents[0]);

            if (!empty($csvContents)) {

                $total = count($csvContents);

                if ($clear) {
                    DB::table('trips')->truncate();
                }

                DB::beginTransaction();

                foreach ($csvContents as $item) {

                    try {

                        Trip::create([
                            'driver_id' => $item[1],
                            'pickup' => $item[2],
                            'dropoff' => $item[3]
                        ]);

                        $processed++;

                    } catch (\Exception $e){
                        DB::rollBack();
                        $message = $e->getMessage();
                    }
                }

                DB::commit();
            }
        }

        $result = [
            'records_total' => $total,
            'records_processed' => $processed,
            'status' => true,
            'message' => $message,
        ];

        return $this->respond($result);

    }

}
