<?php

namespace App\Http\Controllers;

use App\Http\Controllers\API\ITripsListController;
use App\Models\Trip;

use Illuminate\Http\Request;

class TripsListController extends Controller implements ITripsListController
{
    public function list(Request $request){

        $query = Trip::query();

        // searching
        if (!empty($request->get('q'))) {
            $strSearch = $request->get('q');
            $searchFields = Trip::SEARCH_FIELDS;
            $query->where(function ($query) use ($strSearch, $searchFields) {
                foreach ($searchFields as $column) {
                    $relatedColumn = explode('.', $column);
                    if(count($relatedColumn) > 1){
                        $query->orWhereHas($relatedColumn[0], function($query) use ($relatedColumn, $strSearch){
                            $query->where($relatedColumn[1], 'LIKE', "%$strSearch%");
                        });
                    }else{
                        $query->orWhere($column, 'LIKE', "%$strSearch%");
                    }
                }
            });
        }

        // filtering
        if(!empty($request->get('filter_by'))){
            if(!is_array($request->get('filter_by'))){
                return $this->respondBadRequest('The filter_by param is invalid.');
            }

            $query->where(function ($query) use ($request)
            {
                foreach($request->get('filter_by') as $item){
                    $filter = explode(',', $item);
                    $query->orWhere($filter[0], $filter[1]);
                }
            });
        }

        // ordering
        if (!empty($request->get('order_by'))) {
            if (!is_array($request->get('order_by'))) {
                return $this->respondBadRequest('The order_by param is invalid.');
            }
            foreach ($request->get('order_by') as $item) {
                $order = explode(',', $item);
                $query->orderBy($order[0], !empty($order[1]) ? $order[1] : 'asc');
            }
        }

        // paging
        $page = !empty($request->get('page')) ? $request->get('page') : 1;

        // calculate limit
        if ($request->has('limit') && (int)$request->get('limit') === 0 || (int)$request->get('limit') > 100) {
            return $this->respondBadRequest("You can't use unlimited or more then 100 option for pagination");
        }
        $limit = !empty($request->get('limit')) ? (int)$request->get('limit') : 24;

        $countEntries = $query->count();
        if ($countEntries > 0) {
            $responseData = $query->skip(($page - 1) * $limit)
                ->take($limit)
                ->get()
                ->toArray();
        }

        $lastPage = $countEntries % $limit > 0 ? 1 + intval($countEntries / $limit) : intval($countEntries / $limit);

        $result = [
            'total' => $countEntries,
            'per_page' => $limit,
            'current_page' => intval($page),
            'last_page' => $lastPage,
            'result' => !empty($responseData) ? $responseData : []
        ];

        return $this->respond($result);

    }

}
