FROM php:8.0.10-apache
COPY --from=composer:latest /usr/bin/composer /usr/local/bin/composer

RUN apt-get update && \
	apt-get install -y libfreetype6-dev libjpeg62-turbo-dev libpng-dev libzip-dev zip && \
	docker-php-ext-configure gd --with-freetype=/usr/include/ --with-jpeg=/usr/include/ && \
	docker-php-ext-install gd && \
	docker-php-ext-install zip

RUN apt-get install nano -y

RUN docker-php-ext-install pdo pdo_mysql mysqli

RUN rm /etc/apache2/apache2.conf
COPY ./conf/apache2/apache2.conf /etc/apache2/apache2.conf

RUN rm /etc/apache2/sites-available/000-default.conf
COPY ./conf/apache2/000-default.conf /etc/apache2/sites-available/000-default.conf

EXPOSE 3306

RUN a2enmod rewrite
RUN service apache2 restart

ENV COMPOSER_ALLOW_SUPERUSER=1

ENV NODE_VERSION=8.17.0
RUN apt install -y curl
RUN curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.0/install.sh | bash
ENV NVM_DIR=/root/.nvm
RUN . "$NVM_DIR/nvm.sh" && nvm install ${NODE_VERSION}
RUN . "$NVM_DIR/nvm.sh" && nvm use v${NODE_VERSION}
RUN . "$NVM_DIR/nvm.sh" && nvm alias default v${NODE_VERSION}
ENV PATH="/root/.nvm/versions/node/v${NODE_VERSION}/bin/:${PATH}"
RUN node --version
RUN npm --version

#WORKDIR /var/www/html/api/storage/app/
#RUN mkdir tmp
#RUN chmod 777 public

WORKDIR /var/www/html
COPY ./www /var/www/html/

WORKDIR /var/www/html/api
RUN chmod 777 -R storage
RUN composer install
RUN php artisan config:clear

WORKDIR /var/www/html/public
RUN npm install
RUN npm run build-production

EXPOSE 80
