import { Component, OnInit } from '@angular/core';

import { EventsService  } from '../../../services/events/events.service';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SideBarComponent implements OnInit {

  private bodyTag;
  public toggleState: boolean = false;
  public isDisabled: boolean = false;

  constructor(
    private eventsService: EventsService
  ) {

    this.eventsService.getEmitter().subscribe(item => {
      if(item.name === 'layouts-disabled'){
        this.isDisabled = item.data;
      }
    });

  }

  ngOnInit() {
    this.bodyTag = document.getElementsByTagName('body')[0];
  }

  toggle(){
    if(!this.toggleState){
      this.bodyTag.classList.add('sidebar-toggled');
      this.toggleState = true;
    }else{
      this.bodyTag.classList.remove('sidebar-toggled');
      this.toggleState = false;
    }
  }

}
