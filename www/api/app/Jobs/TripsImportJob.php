<?php

namespace App\Jobs;

use App\Models\Trip;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class TripsImportJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var int
     */
    public int $timeout = 1200;

    /**
     * @var array
     */
    public array $header;

    /**
     * @var array
     */
    public array $data;

    public function __construct(array $data, array $header)
    {
        $this->data = $data;
        $this->header = $header;
    }

    /**
     * @return void
     */
    public function handle(): void
    {
        foreach ($this->data as $trip) {
            $tripData = array_combine($this->header,$trip);
            DB::beginTransaction();

            try{

                Trip::create($tripData);

            }catch (\Exception $e){
                DB::rollBack();

                $result = [
                    'records_total' => 0,
                    'records_processed' => 0,
                    'status' => false,
                    'error' => $e->getMessage(),
                ];
            }

            DB::commit();

        }
    }
}
