import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from '../../../material.module';

import { NgxPaginationModule } from 'ngx-pagination';
import { NgxContentLoadingModule } from 'ngx-content-loading';

import { TripsRoutingModule } from './trips-routing.module';

import { TripsComponent } from './trips/trips.component';

@NgModule({
  declarations: [
    TripsComponent
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA
  ],
  imports: [
    FormsModule,
    CommonModule,
    BrowserAnimationsModule,
    MaterialModule,
    NgxPaginationModule,
    NgxContentLoadingModule,
    TripsRoutingModule
  ]
})
export class TripsModule { }
