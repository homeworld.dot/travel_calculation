import { Injectable, isDevMode, NgZone } from '@angular/core';
import { ErrorDialogService } from './error-dialog/errordialog.service';
import {
  HttpInterceptor,
  HttpRequest,
  HttpResponse,
  HttpHandler,
  HttpEvent,
  HttpErrorResponse
} from '@angular/common/http';

import { EventsService } from '../services/events/events.service';


import { Observable, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';

@Injectable()
export class HttpConfigInterceptor implements HttpInterceptor {

  customStatuses: any = [422, 400];

  constructor(
    private eventsService: EventsService,
    public errorDialogService: ErrorDialogService,
    private ngZone: NgZone
  ) { }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    request = request.clone({ headers: request.headers.set('Accept', 'application/json') });

    if(isDevMode()){
      request = request.clone({ headers: request.headers.set('Access-Control-Allow-Headers', '*') });
    }

    return next.handle(request).pipe(
      map((event: HttpEvent<any>) => {
        if (event instanceof HttpResponse) {
          // this.errorDialogService.openDialog(event);
        }
        return event;
      }),
      catchError((res: HttpErrorResponse) => {
        const data = {
          name: res.error ? res.error.status : res.name,
          status: res.error ? res.error.status : res.status,
          statusText: res.statusText ? res.statusText : res.error.statusText,
          message: res.error ? res.error.message : res.message
        };
        if(this.customStatuses.indexOf(data.status) < 0){
          this.ngZone.run(() => {
            this.errorDialogService.openDialog(data);
          });
          return throwError(res);
        }else{
          let errors: any = [];
          if (typeof data.message !== 'string'){
            for(let key in data.message){
              if (data.message.hasOwnProperty(key)) {
                if(data.message[key].length > 0){
                  let msg = '';
                  for(let i = 0; i < data.message[key].length; i++){
                    msg += data.message[key][i];
                  }
                  errors[key] = msg;
                }
              }
            }
          }else{
            errors.email = data.message;
          }
          this.eventsService.emitEvent('request-error', errors);
          return [];
        }

      }));
  }

}
