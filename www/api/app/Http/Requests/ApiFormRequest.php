<?php

namespace App\Http\Requests;

use Illuminate\Http\JsonResponse;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Collection;
use Illuminate\Validation\Factory;

class ApiFormRequest extends FormRequest
{
    public function __construct(Factory $factory)
    {
        parent::__construct();
        $this->useCustomValidations($factory, $this->applicableValidations());
    }

    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [];
    }

    /**
     * @param array $errors
     * @return JsonResponse|RedirectResponse
     */
    public function response(array $errors): JsonResponse|RedirectResponse
    {

        if ($this->expectsJson()) {
            $response = [
                'status' => 'error',
                'code' => 422,
                'message' => $errors
            ];

            if (config('app.debug')){
                $response['exception'] = '\Illuminate\Validation\ValidationException';
            }

            return new JsonResponse($response, 422);
        }

        return $this->redirector->to($this->getRedirectUrl())
            ->withInput($this->except($this->dontFlash))
            ->withErrors($errors, $this->errorBag);
    }

    /**
     * @return Collection
     */
    protected function applicableValidations(): Collection
    {
        return collect([]);
    }


    /**
     * @param $factory
     * @param $validations
     * @return void
     */
    protected function useCustomValidations($factory, $validations): void
    {
        $validations->each(function ($validation) use ($factory) {
            $factory->extend($validation->name(), $validation->test(), $validation->errorMessage());
        });
    }

}
