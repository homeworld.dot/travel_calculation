import { Component, OnInit } from '@angular/core';

import { Config } from '../../../../config/app.config';

import { EventsService } from '../../../../services/events/events.service';
import { RequestsService } from '../../../../services/requests/requests.service';

export interface TripElement {
  id: number;
  driver_id: number;
  pickup: string;
  dropoff: string;
}

@Component({
  selector: 'app-trips',
  templateUrl: './trips.component.html',
  styleUrls: ['./trips.component.scss']
})
export class TripsComponent implements OnInit {

  loading: boolean = false;
  success: boolean = false;
  successMessage: string = '';
  errors: Object = {};

  collection: TripElement[] = [];

  public sorted = {
    id: false,
    driver_id: false,
    pickup: false,
    dropoff: false,
  };
  totalCount: number;
  limit: number;
  currentPage = 1;
  perPage = [];
  orderBy = 'id,asc';
  search = '';
  query = '';

  constructor(
    private config: Config,
    private eventsService: EventsService,
    private requestService: RequestsService,
  ) {
    this.limit = this.config.get('PAGINATION_LIMIT');
    this.perPage = this.config.get('PAGINATION_PAGE_ITEMS');

    this.eventsService.getEmitter().subscribe(item => {
      if(item.name === 'search'){
        this.search = item.data;
        this.currentPage = 1;
        this.fetch();
      }
    });
  }

  ngOnInit() {
    this.fetch();
  }

  fetch(){
    let self = this;

    self.query = '?limit='+this.limit+
        '&page='+this.currentPage+
        '&order_by[]='+this.orderBy+
        '&q='+this.search;

    self.loading = true;

    this.requestService.index('/trips' + self.query).subscribe(
      response => {
        self.totalCount = response.data.total;
        self.collection = response.data.result;
        self.loading = false;
      },
      error => {
        self.loading = false;
      }
    );
  }

  sort(field: string){
    this.orderBy = this.sorted[field] ? field+',asc' : field+',desc';
    this.sorted[field] = !this.sorted[field];
    this.fetch();
  }

  // Pagination
  onPageChange(page){
    this.currentPage = page;
    this.fetch();
  }

  onPerPageChange(value){
    this.limit = value;
    this.currentPage = 1;
    this.fetch();
  }

  import(event) {
    let self = this;
    const file:File = event.target.files[0];

    if (file) {
      const formData = new FormData();

      formData.append("csv", file);
      formData.append("clear", '1');

      this.requestService.createMultipart("/trips", formData).subscribe(
          response => {
            self.fetch();
          },
          error => {
            self.loading = false;
          }
      );

    }
  }

}
