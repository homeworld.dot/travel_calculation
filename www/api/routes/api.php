<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::group([
    'prefix' => 'v1',
    'middleware' => 'api'
], function() {

    Route::get('trips/calculate', 'TripsCalculateController@calculate');
    Route::get('trips', 'TripsListController@list');
    Route::post('trips', 'TripsImportController@importCsv');

});
