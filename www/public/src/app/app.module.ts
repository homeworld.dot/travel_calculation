import { NgModule, CUSTOM_ELEMENTS_SCHEMA  } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { MaterialModule } from './material.module';

import { Config } from './config/app.config';

import { NgxContentLoadingModule } from 'ngx-content-loading';

import { ErrorDialogService } from './interceptor/error-dialog/errordialog.service';
import { RequestsService } from './services/requests/requests.service';
import { GuardService } from './services/guard/guard.service';
import { EventsService } from './services/events/events.service';
import { RouteService } from './services/route/route.service';

import { HttpConfigInterceptor} from './interceptor/httpconfig.interceptor';

import { MatDialogModule } from '@angular/material';

import { TripsModule } from './components/pages/trips/trips.module';
import { DriversModule } from './components/pages/drivers/drivers.module';
import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { ErrorDialogComponent } from './interceptor/error-dialog/errordialog.component';

import { BreadcrumbsComponent } from './components/layouts/breadcrumbs/breadcrumbs.component';
import { HeaderComponent } from './components/layouts/header/header.component';
import { SideBarComponent } from './components/layouts/sidebar/sidebar.component';
import { TopBarComponent } from './components/layouts/topbar/topbar.component';
import { FooterComponent } from './components/layouts/footer/footer.component';

import { NotFoundComponent } from './components/pages/notfound/notfound.component';
import { HomeComponent } from './components/pages/home/home.component';

@NgModule({
  declarations: [
    AppComponent,
    ErrorDialogComponent,
    BreadcrumbsComponent,
    HeaderComponent,
    SideBarComponent,
    TopBarComponent,
    FooterComponent,
    NotFoundComponent,
    HomeComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    MaterialModule,
    HttpClientModule,
    MatDialogModule,
    NgxContentLoadingModule,
    TripsModule,
    DriversModule,
    AppRoutingModule
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA
  ],
  providers: [
    Config,
    ErrorDialogService,
    RequestsService,
    GuardService,
    EventsService,
    RouteService,
    { provide: HTTP_INTERCEPTORS, useClass: HttpConfigInterceptor, multi: true }
  ],
  entryComponents: [ErrorDialogComponent],
  bootstrap: [AppComponent]
})
export class AppModule { }
