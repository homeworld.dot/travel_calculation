import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';


@Injectable()
export class Config {

  private appConfig: object;

  constructor() {
    this.appConfig = {
      ...environment.config,
      ...{
        ALLOWED_PATH: ['auth', 'trips', 'drivers-report'],
        PAGINATION_LIMIT: 24,
        PAGINATION_DIRECTION: 'asc',
        PAGINATION_PAGE_ITEMS: [12, 24, 48, 60, 72, 90]
      }

    };

  }

  get(key: any): any {
    return this.appConfig[key];
  }

}
