import { Component } from '@angular/core';
import { Router, NavigationEnd  } from '@angular/router';

import { EventsService  } from './services/events/events.service';
import { Config } from "./config/app.config";

import { ErrorDialogService } from "./interceptor/error-dialog/errordialog.service";
import { RequestsService } from "./services/requests/requests.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'default';
  public isDisabled = false;

  constructor(
    private router: Router,
    private eventsService: EventsService,
    private requestService: RequestsService,
    private config: Config,
    public errorDialogService: ErrorDialogService
  ) {
    router.events.subscribe((event ) => {
      if (event instanceof NavigationEnd ) {

        this.eventsService.emitEvent('layouts-disabled', false);
        this.isDisabled = false;

      }
    });
  }

}
