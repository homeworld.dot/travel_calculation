<?php

namespace App\Http\Validations;


interface CustomRuleInterface
{
    public function name();
    public function test();
    public function errorMessage();

}
