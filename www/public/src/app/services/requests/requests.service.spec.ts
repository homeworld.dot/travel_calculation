import { TestBed } from '@angular/core/testing';

import { HttpClient, HttpHandler } from '@angular/common/http';
import { Config } from '../../config/app.config';
import { RequestsService } from './requests.service';

describe('RequestsService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    providers: [
      Config,
      HttpClient,
      HttpHandler,
      RequestsService
    ]
  }));

  it('should be created', () => {
    const service: RequestsService = TestBed.get(RequestsService);
    expect(service).toBeTruthy();
  });
});
