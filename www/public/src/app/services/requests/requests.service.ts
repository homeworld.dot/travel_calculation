import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';

import { Config } from '../../config/app.config';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
};

@Injectable()
export class RequestsService {

  API_URL: string;

  constructor(
    private config: Config,
    private http: HttpClient
  ) {
    this.API_URL = this.config.get('API_URL');
  }

  downloadFile(uri: string): any {
    return this.http.get(this.API_URL + uri, { responseType: 'blob' });
  }

  index(uri: string): any {
    return this.http.get(this.API_URL + uri, httpOptions);
  }

  show(uri: string, id: any) {
    return this.http.get(this.API_URL + uri + id, httpOptions);
  }

  create(uri: string, data: any) {
    return this.http.post(this.API_URL + uri, data, httpOptions);
  }

  createMultipart(uri: string, data: any) {
    return this.http.post(this.API_URL + uri, data);
  }

  update(uri: string, id: number, model: any) {
    const data = JSON.stringify(model);
    return this.http.put(this.API_URL + uri + id, data, httpOptions);
  }

  updateEmpty(uri: string, id: number, pass: string) {
    return this.http.put(this.API_URL + uri + id + pass, httpOptions);
  }

  updateMultipart(uri: string, id: number, model: any) {
    return this.http.post(this.API_URL + uri + id, model);
  }

  delete(uri: string, id: number) {
    return this.http.delete(this.API_URL + uri + id);
  }

}
