<table style="width:100%;font-size:17px;">
    <thead>
    <tr>
        <th style="background:#242424 url({{env('SITE_URL')}}/images/watermark.png) no-repeat 20px center;height:80px;"></th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td style="padding:10px 20px;">Здравствуйте, {{ $member->name }}</td>
    </tr>
    <tr>
        <td style="padding:10px 20px;">
            <p>Вы только что прошли регистрацию на <a style="color:#039be5;" href="{{env('SITE_URL')}}" target="_blank">console-bay.com</a><p>
        </td>
    </tr>
    <tr>
        <td style="padding:10px 20px;">Для активации аккаунта используйте код: {{$member->activation}}</td>
    </tr>
    </tbody>
    <tfoot>
    <tr>
        <td style="background:#242424;height:30px;"></td>
    </tr>
    </tfoot>
</table>


