import {EventEmitter, Injectable} from '@angular/core';


export class Message {
  constructor( public name: string, public data: any) {

  }

}
@Injectable()
export class EventsService {

  dispatcher: EventEmitter<any> = new EventEmitter();

  constructor() {}

  emitEvent(name: string, data: any) {
    const message = new Message(name, data);
    this.dispatcher.emit(message);
  }

  getEmitter() {
    return this.dispatcher;
  }

}

