import { Component, Inject } from '@angular/core';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material';

import { timer } from 'rxjs';
import { take, map } from 'rxjs/operators';

@Component({
  selector: 'app-root',
  templateUrl: './errordialog.component.html',
  styleUrls: ['./errordialog.component.scss']
})
export class ErrorDialogComponent {

  public countDown: any;
  public count: number = 5;

  modalCloseCodes = [400, 401, 405];

  constructor(

    @Inject(MAT_DIALOG_DATA)
    public data: any,
    private dialog: MatDialog

  ) {

    this.countDown = timer(0,1000).pipe(
      take(this.count),
      map(() => {
        --this.count;
       
        if(this.count == 0){
          this.dialog.closeAll();
        }

      })
    );

  }

}
