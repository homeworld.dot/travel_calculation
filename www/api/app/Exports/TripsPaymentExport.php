<?php

namespace App\Exports;

use Maatwebsite\Excel\Excel;
use Illuminate\Contracts\Support\Responsable;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\Exportable;

class TripsPaymentExport implements FromArray, Responsable
{
    use Exportable;

    /**
     * @var string
     */
    private string $fileName = 'trips_payment.csv';

    /**
     * @var string
     */
    private string $writerType = Excel::CSV;

    /**
     * @var string[]
     */
    private array $headers = [
        'Content-Type' => 'text/csv',
    ];

    /**
     * @var array
     */
    protected array $array = [];

    public function array(): array
    {
        return $this->array;
    }

    /**
     * @param array $array
     * @return void
     */
    public function setArray(array $array): void
    {
        $this->array = $array;
    }
}
