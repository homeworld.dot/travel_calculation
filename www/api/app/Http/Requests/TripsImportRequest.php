<?php

namespace App\Http\Requests;

use App\Http\Validations\CheckCSVFile;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Route;


class TripsImportRequest extends ApiFormRequest
{
    public function rules(): array
    {
        $routeSegments = explode('@', Route::currentRouteAction());

        switch (end($routeSegments)){
            case 'importCsv':
                $rules = array_merge(self::customRules(),  self::importCsvRules());
                break;

            default:
                $rules = self::customRules();
                break;
        }

        return $rules;
    }

    /**
     * @return array
     */
    public static function customRules(): array
    {
        return [];
    }

    /**
     * @return string[]
     */
    public static function importCsvRules()
    {
        return [
            'csv' => 'required|file|check_csv_file:importCsv'
        ];

    }

    public function messages()
    {
        return [
            'csv.required' => 'CSV file is required.',
            'csv.mime' => 'Wrong file format.'
        ];
    }

    /**
     * @return Collection
     */
    protected function applicableValidations(): Collection
    {
        return collect([
            new CheckCSVFile()
        ]);
    }
}
