import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd, ActivatedRoute  } from '@angular/router';

import { filter } from 'rxjs/operators';

import { EventsService  } from '../../../services/events/events.service';
import { RouteService } from '../../../services/route/route.service';


@Component({
  selector: 'app-breadcrumbs',
  templateUrl: './breadcrumbs.component.html',
  styleUrls: ['./breadcrumbs.component.scss']
})
export class BreadcrumbsComponent{

  public isDisabled: boolean = false;

  pages: any = [];

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private routeService: RouteService,
    private eventsService: EventsService
  ) {

    this.eventsService.getEmitter().subscribe(item => {
      if(item.name === 'layouts-disabled'){
        this.isDisabled = item.data;
      }
    });

    this.router.events.pipe(
      filter(event => event instanceof NavigationEnd)
    ).subscribe(() => {
      this.pages = this.activatedRoute.root.firstChild.snapshot.data['breadcrumb'];
    });

  }

  ngOnInit() {

  }

}
