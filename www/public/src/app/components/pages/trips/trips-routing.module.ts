import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { GuardService } from '../../../services/guard/guard.service';

import { TripsComponent } from './trips/trips.component';


const routes: Routes = [
  {
    path: 'trips',
    component: TripsComponent,
    canActivate: [GuardService],
    data: {
      breadcrumb: [
        {title: 'Trips', url: null}
      ]
    }
  }
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TripsRoutingModule { }
