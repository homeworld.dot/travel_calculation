import { Injectable } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

import { Router } from '@angular/router';
import { ErrorDialogComponent } from './errordialog.component';

import { EventsService } from '../../services/events/events.service';

@Injectable()
export class ErrorDialogService {

  public statuses = {
    0: {
      title: 'Server error',
      message: 'Unknown server error. ',
      text: 'Modal will close in ',
      code: 0
    },
    400: {
      title: 'Bad request',
      message: 'Check you request data. Modal will close in ',
      text: 'Modal will close in ',
      code: 400
    },
    401: {
      title: 'Unauthorized access detected',
      message: 'You will be redirected to authorization page in ',
      text: 'You will be redirected to authorization page in ',
      code: 401
    },
    404: {
      title: 'Not found',
      message: 'Entity not found. ',
      text: 'You will be redirected to previous page in ',
      code: 404
    },
    403: {
      title: 'No permission',
      message: 'You have no permission to access this feature. ',
      text: 'Modal will close in ',
      code: 403
    },
    405: {
      title: 'Not allowed',
      message: 'You have no permission to access this feature. ',
      text: 'Modal will close in ',
      code: 405
    }
  };

  constructor(
    public dialog: MatDialog,
    private router: Router,
    private eventsService: EventsService
  ) {

  }

  openDialog(data): void {
    let errorData = this.statuses[data.status] ? this.statuses[data.status] : this.statuses[0];
    let errors: any = [];
    if(data.message){
      if (typeof data.message !== 'string'){
        for (let key in data.message){
          if (data.message.hasOwnProperty(key)) {
            if(data.message[key].length > 0){
              let msg = '';
              for(let i = 0; i < data.message[key].length; i++){
                msg += data.message[key][i];
              }
              errors[key] = msg;
            }
          }
        }
      }else{
        errorData.message = data.message;
      }
    }

    let dialogRef = this.dialog.open(ErrorDialogComponent, {width: '480px', data: errorData});

    dialogRef.afterClosed().subscribe(result => {

      switch (errorData.code) {

        case 404:
          this.router.navigate(['']);
          break;

        case 400:
          this.eventsService.emitEvent('request-error', errors);
          break;
        case 405:
          break;

        case 0:
          this.eventsService.emitEvent('request-error', {});
          break;

        default:
          break;

      }

    });

  }
}
