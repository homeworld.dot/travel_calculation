<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;

interface ITripsCalculateController
{

    /**
     * @api {get} /trips/calculate List of trips
     * @apiDescription Get list of Trips.
     *
     * @apiName CalculateTrips
     * @apiGroup Trips
     * @apiVersion 1.0.0
     *
     * @apiParam  {Number=0,1} [export] Export result to csv file.
     *
     * @apiSuccessExample Success-Response:
     * HTTP/1.1 200 OK
     *{
     *  "status": "success",
     *  "code": 200,
     *  "data": [
     *      {
     *          "driver_id": 12
     *          "total_minutes_with_passenger": 476
     *      },
     *      {...}
     *  ]
     *}
     *
     * @apiUse BadRequestException
     *
     */
    public function calculate(Request $request);
}
