<?php

namespace App\Http\Controllers;

use App\Exports\TripsPaymentExport;
use App\Http\Controllers\API\ITripsCalculateController;
use App\Models\Trip;
use App\Services\TripsCalculationService;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Maatwebsite\Excel\Excel;

class TripsCalculateController extends Controller implements ITripsCalculateController
{
    protected TripsCalculationService $tripsCalculationService;

    public function __construct(TripsCalculationService $tripsCalculationService)
    {
        $this->tripsCalculationService = $tripsCalculationService;
    }

    public function calculate(Request $request){

        $export = $request->has('export') && $request->get('export');

        $result = $this->tripsCalculationService->calculate($export);

        if ($export) {
            return $this->tripsCalculationService->export($result);
        }

        return $this->respond(array_values($result));

    }

}
