import { Component, OnInit } from '@angular/core';

import { EventsService  } from '../../../services/events/events.service';
import { NavigationEnd, Router, ActivatedRoute} from "@angular/router";

@Component({
  selector: 'app-topbar',
  templateUrl: './topbar.component.html',
  styleUrls: ['./topbar.component.scss']
})
export class TopBarComponent implements OnInit {

  private disabledPages: any = [
    'HomeComponent',
  ];

  public isDisabled: boolean = false;
  public isSearch: boolean = true;

  public user: object;
  public search: string = '';

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private eventsService: EventsService,
  ) {

    this.eventsService.getEmitter().subscribe(item => {
      if(item.name === 'layouts-disabled'){
        this.isDisabled = item.data;
      }
    });

  }

  ngOnInit() {
    this.router.events.subscribe((event ) => {
      if (event instanceof NavigationEnd ) {
        this.route.snapshot.children.map(e => {
          this.isSearch = this.disabledPages.indexOf(e.component['name']) < 0;
        });
      }
    });

  }

  public startSearch(){
    this.eventsService.emitEvent('search', this.search);
  }
  public clearSearch(){
    this.search = '';
    this.eventsService.emitEvent('search', this.search);
  }

  public logout(){
    this.router.navigate(['auth']);
  }

}
