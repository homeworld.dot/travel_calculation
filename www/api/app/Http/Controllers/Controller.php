<?php

namespace App\Http\Controllers;

use App\Http\Traits\RespondsTrait;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
/**
 * Global errors definition
 *
 */

/**
 * @apiDefine BadRequestException
 * @apiError (400) BadRequestException Bad params to request
 * @apiErrorExample BadRequest-Exception
 *  HTTP/1.1 400 Bad Request
 *  {
 *      "status": "error",
 *      "code": 400,
 *      "message": "Bad params to request"
 *  }
 */

/**
 * @apiDefine ValidationException
 * @apiError (422) ValidationException The field validation error
 * @apiErrorExample Validation-Exception
 *  HTTP/1.1 422 Bad Request
 *  {
 *      "status": "error",
 *      "code": 422,
 *      "message": {
 *           "email": [
 *               "The email has already been taken."
 *           ],
 *           "uid": [
 *               "The uid has already been taken."
 *           ]
 *       },
 *      "exception": "\\Illuminate\\Validation\\ValidationException"
 *  }
 */

/**
 * @apiDefine UnauthorizedException
 * @apiError (401) UnauthorizedException Unauthorized request
 * @apiErrorExample Unauthorized-Exception
 *  HTTP/1.1 401 Unauthorized
 *  {
 *      "status": "error",
 *      "code": 401,
 *      "message": "Unauthorized"
 *  }
 */

/**
 * @apiDefine MethodNotAllowedException
 * @apiError (405) MethodNotAllowedException Method not allowed
 * @apiErrorExample {json} NotAllowed-Exception
 *     HTTP/1.1 405 Not allowed
 *     {
 *          "status" : "error",
 *          "code": 405,
 *          "message" : "Not allowed",
 *     }
 */

/**
 * @apiDefine ForbiddenException
 * @apiError (403) ForbiddenException Forbidden
 * @apiErrorExample {json} Forbidden-Exception
 *     HTTP/1.1 403 Forbidden
 *     {
 *          "status" : "error",
 *          "code": 403,
 *          "message" : "Forbidden",
 *     }
 */

/**
 * @apiDefine NotFoundException
 * @apiError (404) NotFoundException Entity not found
 * @apiErrorExample {json} NotFound-Exception
 * HTTP/1.1 404 Not Found
 * {
 *  "status" : "error",
 *  "code": 404,
 *  "message" : "Entity not found",
 * }
 */

/**
 * @apiDefine InternalErrorException
 * @apiError (500) InternalErrorException InternalE system error
 * @apiErrorExample {json} InternalError-Exception
 *     HTTP/1.1 500 Not Found
 *     {
 *          "status" : "error",
 *          "code": 500,
 *          "message" : "Missing controller attribute.",
 *     }
 */


/**
 * Global headers definition
 *
 */

/**
 * @apiDefine RequestsHeaders
 *
 * @apiHeader (Headers) {String} X-Requested-With   To identify Ajax requests, should be equal "XMLHttpRequest"
 * @apiHeader (Headers) {String} Content-Type       Content type header, should be equal "application/x-www-form-urlencoded" or "application/json"
 * @apiHeaderExample {json} X-Requested-With-Header-Example:
 * {
 *    "X-Requested-With": "XMLHttpRequest"
 * }
 * @apiHeaderExample {json} Content-Type-Header-Example:
 * {
 *    "Content-Type": "application/x-www-form-urlencoded"
 * }
 *
 */

/**
 * @apiDefine AuthorizationHeader
 *
 * @apiHeader (Headers) {String} Authorization  Authorization value, should be equal "Bearer MpyefTuOGz3W08PyptY6QHHg56JWIQT3TXLErvIo0Pf3mqqWuHcIyr9RQ8Bb"
 * @apiHeaderExample {json} Authorization-Header-Example:
 * {
 *    "Authorization": "Bearer MpyefTuOGz3W08PyptY6QHHg56JWIQT3TXLErvIo0Pf3mqqWuHcIyr9RQ8Bb"
 * }
 *
 */
class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests, RespondsTrait;

    function getUserIP()
    {
        $client  = @$_SERVER['HTTP_CLIENT_IP'];
        $forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
        $remote  = $_SERVER['REMOTE_ADDR'];

        if(filter_var($client, FILTER_VALIDATE_IP)){
            $ip = $client;
        }elseif(filter_var($forward, FILTER_VALIDATE_IP)){
            $ip = $forward;
        }else{
            $ip = $remote;
        }

        return $ip;
    }


}
