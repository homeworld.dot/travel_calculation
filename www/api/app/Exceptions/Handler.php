<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Symfony\Component\HttpFoundation\Response as ResponseAlias;
use Throwable;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param Throwable $e
     * @return void
     * @throws Exception
     * @throws Throwable
     */
    public function report(Throwable $e): void
    {
        parent::report($e);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  Request  $request
     * @param Throwable $e
     * @return ResponseAlias
     *
     * @throws Throwable
     */
    public function render($request, Throwable $e)
    {
        // If the request wants JSON (AJAX doesn't always want JSON)
        if ($request->is('api/*') || $request->wantsJson())
        {
            // hook for Validation Exception
            if($e instanceof \Illuminate\Validation\ValidationException)
            {
                return self::validationExceptionResponse($request, $e);
            }

            // hook for 22P02 PDO exception
            if($e instanceof \Illuminate\Database\QueryException && $e->getCode() === '22P02')
            {
                $response = [
                    'status'  => 'error',
                    'message' => 'Not found.'
                ];

                $response['code'] = method_exists($e, 'getStatusCode') ? $e->getStatusCode() : ResponseAlias::HTTP_NOT_FOUND;
            }else{
                // Define the response
                $response = [
                    'status'  => 'error',
                    'message' => 'Sorry, something went wrong.'
                ];

                // Default response of Response::HTTP_BAD_REQUEST = 400
                $response['code'] = method_exists($e, 'getStatusCode') ? $e->getStatusCode() : ResponseAlias::HTTP_BAD_REQUEST;

            }

            // If this exception is an instance of HttpException
            if ($this->isHttpException($e))
            {
                // Grab the HTTP status code from the Exception
                $response['code'] = $e->getStatusCode();
            }


            // If the app is in debug mode
            if (config('app.debug'))
            {
                // Add the exception class name, message and stack trace to response
                $response['exception'] = get_class($e); // Reflection might be better here
                $response['message'] = !empty($e->getMessage()) ? $e->getMessage() : Response::$statusTexts[$response['code']] ;
                $response['error']['trace'] = $e->getTrace();
            }

            // Return a JSON response with the response array and status code
            return response()->json($response, $response['code']);
        }

        return parent::render($request, $e);
    }

    protected static function validationExceptionResponse($request, $exception)
    {
        $response = [
            'status' => 'error',
            'code' => 422,
            'message' => $exception->errors(),
            'error' => ['trace' => $exception->getTrace()]
        ];

        if (config('app.debug')){
            $response['exception'] = '\Illuminate\Validation\ValidationException';
        }

        return response()->json($response, ResponseAlias::HTTP_UNPROCESSABLE_ENTITY);
    }
}
