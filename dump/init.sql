SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

CREATE DATABASE IF NOT EXISTS travel_calculation;

USE travel_calculation;

-- ----------------------------
-- Table structure for trips
-- ----------------------------
DROP TABLE IF EXISTS `trips`;
CREATE TABLE `trips`  (
 `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
 `driver_id` int NULL DEFAULT NULL,
 `pickup` datetime(0) NULL DEFAULT NULL,
 `dropoff` datetime(0) NULL DEFAULT NULL,
 `created` timestamp(0) NOT NULL DEFAULT current_timestamp(0),
 `modified` timestamp(0) NULL DEFAULT NULL,
 PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;

