Task description:
	
	Create the test App for the calculation of the payable time of drivers. Payable is the time when at
	least one passenger is in the vehicle. The vehicle can contain several passengers at the same time.
	
	Input Data:
	
		trips.csv contains the trip data with the following columns:

	Output Data:
		A file with the next data:
	
	Requirements:
		Will be a Plus:
			• id - ID of the trip for one passenger;
			• driver_id - the driver ID;
			• pickup - the date and time of the passenger pickup;
			• dropoff - the date and time of the passenger drop off.
			• driver_id - the driver ID;
			• total_minutes_with_passenger - the total time when at least one passenger was in thevehicle.
			
1. Create a Web service for generating output data based on input data. The service should read the input data from a database (trips.csv should be imported into the database).

2. Technologies:
	• PHP
	• Laravel/Phalcon
	• MySQL
	
3. UI Page:
	a. Data Table: Trips
		• Columns - all from trips.csv (see Input Data above)
		• Features: search, ordering
	b. Data Table: Drivers Report
		• Columns: Driver ID, Payable Time (minutes)
		• Features: search, ordering
	c. Button: Calculate Payable Time
		• When the button is clicked then Payable Time is calculated and filled in the table
	Technologies:
		• Angular and Telerik (preferable), or jQuery

How to run:

1. Run $ docker-compose up -d

2. Open http://locahost to see Trips&Drive application
	
3. Open http://localhost:81 to see phpmyadmin
	- user: root
	- password: test
	
4. Use trips.csv to import data 
