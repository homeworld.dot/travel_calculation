import { Injectable } from '@angular/core';
import { Router, NavigationEnd, CanActivate } from '@angular/router';

import { Config } from '../../config/app.config';

@Injectable()
export class GuardService implements CanActivate{

  allowedPaths: any = [];

  constructor(
    private router: Router,
    private config: Config,
  ) {
    this.allowedPaths = this.config.get('ALLOWED_PATH');
  }

  canActivate() {
    let path = window.location.href.split('/');
    return true;
  }
}
