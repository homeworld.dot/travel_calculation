<?php

namespace App\Http\Traits;

use Illuminate\Http\Response;
use Symfony\Component\HttpFoundation\Response as ResponseAlias;


/**
 * These methods are typically the same across all guards.
 */
trait RespondsTrait
{
    /**
     * @var int
     */
    protected int $statusCode = ResponseAlias::HTTP_OK;

    /**
     * @return int
     */
    public function getStatusCode(): int
    {
        return $this->statusCode;
    }


    /**
     * @param $statusCode
     * @return $this
     */
    public function setStatusCode($statusCode): static
    {

        $this->statusCode = $statusCode;

        return $this;
    }


    /**
     * @param $data
     * @param array $headers
     * @return mixed
     */
    public function respondCreated($data, array $headers = []): mixed
    {
        return $this->setStatusCode(ResponseAlias::HTTP_CREATED)->respond($data, $headers);
    }


    /**
     * @param array $data
     * @param array $headers
     * @return mixed
     */
    public function respondNoContent(array $data = [], array $headers = []): mixed
    {
        return $this->setStatusCode(ResponseAlias::HTTP_NO_CONTENT)->respond($data, $headers);
    }


    /**
     * @param string|null $message
     * @return mixed
     */
    public function respondBadRequest(string $message = null): mixed
    {
        return $this->setStatusCode(ResponseAlias::HTTP_BAD_REQUEST)->respondWithErrors($message ?: Response::$statusTexts[ResponseAlias::HTTP_BAD_REQUEST]);
    }


    /**
     * @param string|null $message
     * @return mixed
     */
    public function respondUnauthorized(string $message = null): mixed
    {
        return $this->setStatusCode(ResponseAlias::HTTP_UNAUTHORIZED)->respondWithErrors($message ?: Response::$statusTexts[ResponseAlias::HTTP_UNAUTHORIZED]);
    }


    /**
     * @param string|null $message
     * @return mixed
     */
    public function respondForbidden(string $message = null): mixed
    {
        return $this->setStatusCode(ResponseAlias::HTTP_FORBIDDEN)->respondWithErrors($message ?: Response::$statusTexts[ResponseAlias::HTTP_FORBIDDEN]);
    }


    /**
     * @param string|null $message
     * @return mixed
     */
    public function respondNotFound(string $message = null){
        return $this->setStatusCode(ResponseAlias::HTTP_NOT_FOUND)->respondWithErrors($message ?: Response::$statusTexts[ResponseAlias::HTTP_NOT_FOUND]);
    }


    /**
     * @param string|null $message
     * @return mixed
     */
    public function respondInternalError(string $message = null): mixed
    {
        return $this->setStatusCode(ResponseAlias::HTTP_INTERNAL_SERVER_ERROR)->respondWithErrors($message ?: Response::$statusTexts[ResponseAlias::HTTP_INTERNAL_SERVER_ERROR]);
    }


    /**
     * @param string|null $message
     * @return mixed
     */
    public function respondNotAllowed(string $message = null){
        return $this->setStatusCode(ResponseAlias::HTTP_METHOD_NOT_ALLOWED)->respondWithErrors($message ?: Response::$statusTexts[ResponseAlias::HTTP_METHOD_NOT_ALLOWED]);
    }

    /**
     * @param string|null $messages
     * @return mixed
     */
    public function respondValidationException(string $messages = null): mixed
    {
        return $this->setStatusCode(ResponseAlias::HTTP_UNPROCESSABLE_ENTITY)->respondWithErrors($messages ?: 'Validation errors');
    }

    /**
     * @param string|null $message
     * @return mixed
     */
    public function respondUnprocessableEntity(string $message = null): mixed
    {
        return $this->setStatusCode(ResponseAlias::HTTP_UNPROCESSABLE_ENTITY)->respondWithErrors($message ?: Response::$statusTexts[ResponseAlias::HTTP_UNPROCESSABLE_ENTITY]);
    }

    /**
     * @param $message
     * @return mixed
     */
    public function respondNotAcceptable($message = null): mixed
    {
        return $this->setStatusCode(ResponseAlias::HTTP_NOT_ACCEPTABLE)->respondWithErrors($message ?: Response::$statusTexts[ResponseAlias::HTTP_NOT_ACCEPTABLE]);
    }

    /**
     * Success response
     *
     * @param array $data
     * @param array $headers
     * @return mixed
     */
    public function respond(array $data, array $headers = []): mixed
    {
        return \Response::json(
            [
                'status' => 'success',
                'code'   => $this->getStatusCode(),
                'data' => $data
            ],
            $this->getStatusCode(),
            $headers
        );
    }


    /**
     * @param array $data
     * @param array $headers
     * @param $code
     * @return mixed
     */
    public function respondWithErrors(array $data, array $headers = [], $code = null): mixed
    {
        if($code){
            $this->setStatusCode($code);
        }

        return \Response::json(
            [
                'status' => 'error',
                'code'   => $this->getStatusCode(),
                'message' => $data
            ],
            $this->getStatusCode(),
            $headers
        );
    }


}
