<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;

interface ITripsListController
{

    /**
     * @api {post} /trips Import trips
     * @apiDescription Import Trips data from csv file.
     *
     * @apiName GetTrips
     * @apiGroup Trips
     * @apiVersion 1.0.0
     *
     * @apiParam  {File} [q] Optional, search string.
     * @apiParam  {Number} [limit=24] Optional limit the response the specified number of records
     * @apiParam  {Number} [page=1] Optional specify the page number.
     * @apiParam  {String} [filter_by[]]  Optional specify the field to filter by. Example: filter_by[]=driver_id
     * @apiParam  {String} [order_by[]] Optional specify the field to sorting by. Example: order_by[]=driver_id
     *
     * @apiSuccessExample Success-Response:
     * HTTP/1.1 200 OK
     *{
     *  "status": "success",
     *  "code": 200,
     *  "data": {
     *      "total": 3,
     *      "per_page": 24,
     *      "current_page": 1,
     *      "last_page": 1,
     *      "result": [
     *          {
     *              "id": 1,
     *              "driver_id": "Игровые приставки",
     *              "pickup": "28-08-2015 11:42:30",
     *              "dropoff": "28-08-2015 11:42:30",
     *              "created_at": "28-08-2015 11:42:30",
     *              "modified_at": "30-07-2016 11:18:32",
     *          },
     *          {...},
     *     ]
     *  }
     *}
     *
     * @apiUse BadRequestException
     *
     */
    public function list(Request $request);
}
