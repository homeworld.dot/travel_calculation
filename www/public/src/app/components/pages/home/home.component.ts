import { Component, OnInit } from '@angular/core';

import { Config } from '../../../config/app.config';

import { EventsService } from '../../../services/events/events.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  isDisabled: boolean = false;

    constructor(
      private config: Config,
      private eventsService: EventsService,
  ) {
      this.eventsService.getEmitter().subscribe(item => {
        if(item.name === 'layouts-disabled'){
          this.isDisabled = item.data;
        }
      });
  }

  ngOnInit() {

  }

}
